/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include "editprod.h"
#include "mglob.h"
#include "mainwindow.h"


EditProd::EditProd(QWidget *parent) : LoadProd(parent)
{}

EditProd::EditProd(QWidget *parent, MainWindow *window) : LoadProd(parent)
{
    MINIT

    setWindowTitle("Modifica Prodotti");
    lineEdit_Img->setDisabled(true);
    pushButton_Img->setDisabled(true);

    connect(pushButton_Img, SIGNAL(clicked()), this, SLOT(findImg()));
    connect(comboBox_Id, SIGNAL(activated(int)), this, SLOT(fill(int)));

}

void EditProd::appendData()
{
    if(isNotValid()) return;

    OP("aggiorna")

    QString imgLocal(lineEdit_Img->text());
    QString presente("0");
    if(checkBox_Presente->isChecked()) presente.setNum(1);

    QDomElement item(tmp.createElement("item"));

    item.setAttribute("table",              "prodotti");
    item.setAttribute("id",                 comboBox_Id->itemData(comboBox_Id->currentIndex()).toString());
    item.setAttribute("genere",             "p");
    item.setAttribute("nome",               lineEdit_Name->text());
    item.setAttribute("prezzo",             lineEdit_Price->text());
    item.setAttribute("prezzopromozionale", lineEdit_PromPrice->text());
    item.setAttribute("presente",           presente);
    item.setAttribute("descrizione",        textEdit_Descr->toPlainText().replace("\"","%26quot;"));
    item.setAttribute("where",              whereProd);
    //item.setAttribute("img",                "prod_" + dbId + imgLocal.right(imgLocal.size()- imgLocal.lastIndexOf(".")));
    //item.setAttribute("imgLocal",           imgLocal);


    tmp.loock();

    op.appendChild(item);

    item = tmp.createElement("item");

    item.setAttribute("table",              "prodoTipi");
    item.setAttribute("idProd",             comboBox_Id->itemData(comboBox_Id->currentIndex()).toString());
    item.setAttribute("idTipo",             comboBox_Tipo->itemData(comboBox_Tipo->currentIndex()).toString());
    item.setAttribute("where",              whereType);

    op.appendChild(item);

}

void EditProd::asNew()
{  
    dbId = 0;
    xmlData.clear();

    lineEdit_PromPrice->clear();
    lineEdit_Price->clear();
    lineEdit_Name->clear();
    lineEdit_Img->clear();
    textEdit_Descr->clear();
    checkBox_Presente->setChecked(false);
    comboBox_Tipo->clear();
    comboBox_Id->clear();

    connect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));

    doQuery("<request><op id='view'><item table='tipi' /><item table='prodotti' /><item table='prodoTipi' /></op></request>");
}

void EditProd::populate()
{
    QDomNodeList items(xmlData.elementsByTagName("item"));

    for(int i=0; i<items.size(); i++ )
    {
       if(items.at(i).isElement())
       {
           QDomElement item(items.at(i).toElement());

           QString idProd(item.attributeNode("id").value());
           QString visualName(idProd + " - " + item.attributeNode("nome").value());
           if(idProd != "") comboBox_Id->addItem(visualName, idProd);

           QString idTipo(item.attributeNode("idTipo").value());
           QString nomeTipo(item.attributeNode("nomeTipo").value());
           if( idTipo != "" && nomeTipo != "")
               comboBox_Tipo->addItem(nomeTipo, idTipo);
       }
    }

    disconnect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));
}


void EditProd::fill(int index)
{

    QDomElement item(xmlData.gelementById(comboBox_Id->itemData(index).toString()));

    lineEdit_Name->setText(item.attributeNode("nome").value());
    lineEdit_Img->setText(item.attributeNode("img").value());
    lineEdit_Price->setText(item.attributeNode("prezzo").value());
    lineEdit_PromPrice->setText(item.attributeNode("prezzopromozionale").value());
    textEdit_Descr->setText(item.attributeNode("descrizione").value());
    checkBox_Presente->setChecked(item.attributeNode("presente").value().toInt());
    whereProd = item.attributeNode("where").value();

    QDomNodeList items(xmlData.elementsByTagName("item"));

    for(int i=0; i<items.size(); i++ )
    {
        if(items.at(i).isElement())
        {
            QDomElement item(items.at(i).toElement());

            if(item.attributeNode("table").value() == "prodoTipi" && item.attributeNode("idProd").value() == comboBox_Id->itemData(index).toString())
            {
                comboBox_Tipo->setCurrentIndex(comboBox_Tipo->findData(item.attributeNode("idTipo").value()));
                whereType = item.attributeNode("where").value();
            }
        }
    }
}
