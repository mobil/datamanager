/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QtDebug>
#include <QDomElement>
#include <QTimer>

#include "mainwindow.h"
#include "mglob.h"


MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->tabWidget->clear();
    ui->logPlainTextEdit->setReadOnly(true);

    logIndex = 0;

    ftpPush = 0;
    httpPush.setHost(MGlob::dbHost(), MGlob::dbPort());

    loadProd        = new LoadProd        (ui->tabWidget, this); loadProd->hide();
    loadCompo       = new LoadCompo       (ui->tabWidget, this); loadCompo->hide();
    loadCatalogs    = new LoadCatalogs    (ui->tabWidget, this); loadCatalogs->hide();
    loadType        = new LoadType        (ui->tabWidget, this); loadType->hide();
    loadTrademarks  = new LoadTrademarks  (ui->tabWidget, this); loadTrademarks->hide();
    loadTags        = new LoadTags        (ui->tabWidget, this); loadTags->hide();

    editProd        = new EditProd        (ui->tabWidget, this); editProd->hide();
    editSettings    = new EditSettings    (ui->tabWidget, this); editSettings->hide();

    linkProdCatalog = new LinkProdCatalog (ui->tabWidget, this); linkProdCatalog->hide();
    linkProdCompo   = new LinkProdCompo   (ui->tabWidget, this); linkProdCompo->hide();
    linkProdTag     = new LinkProdTag     (ui->tabWidget, this); linkProdTag->hide();
    linkProdMark    = new LinkProdMark    (ui->tabWidget, this); linkProdMark->hide();

    deleteProd      = new DeleteProd      (ui->tabWidget, this); deleteProd->hide();
    deleteType      = new DeleteType      (ui->tabWidget, this); deleteType->hide();
    deleteTag       = new DeleteTag       (ui->tabWidget, this); deleteTag->hide();
    deleteTradeMark = new DeleteTradeMark (ui->tabWidget, this); deleteTradeMark->hide();
    deleteCatalog   = new DeleteCatalog   (ui->tabWidget, this); deleteCatalog->hide();

    advancedEditor  = new AdvancedEditor  (ui->tabWidget, this); advancedEditor->hide();


    progressDialog = new QProgressDialog(this); progressDialog->setModal(true); progressDialog->hide();
    progressDialog->setMinimumWidth(400);
    progressDialog->setCancelButton(NULL);


    clearXmlRequest();

    connect(ui->listWidget, SIGNAL(currentRowChanged (int)), this,SLOT(switchAction(int)));
    connect(ui->pushButton_Save, SIGNAL(clicked()), this, SLOT(appendData()));
    connect(ui->pushButton_Cancel, SIGNAL(clicked()), this, SLOT(clearXmlRequest()));


}

MainWindow::~MainWindow()
{}

void MainWindow::switchAction(int currentRow)
{
    //qWarning() << "MainWindow::actionClicked called "<< currentRow << endl;
    ui->tabWidget->clear();
    switch(currentRow)
    {
        case 0:                     // Products
            SHOW(loadProd)
            SHOW(editProd)
            SHOW(deleteProd)
            break;
        case 1:                     // Compositions
            SHOW(loadCompo)
            SHOW(linkProdCompo)
            break;

        case 2:                     // Catalogs
            SHOW(loadCatalogs)
            SHOW(linkProdCatalog)
            SHOW(deleteCatalog)
            break;

        case 3:                     // Types
            SHOW(loadType)
            SHOW(deleteType)
            break;

        case 4:                     // Trademarks
            SHOW(loadTrademarks)
            SHOW(linkProdMark)
            SHOW(deleteTradeMark)
            break;

        case 5:                     // Tags
            SHOW(loadTags)
            SHOW(linkProdTag)
            SHOW(deleteTag)
            break;

        case 6:                     // Advanced
            SHOW(advancedEditor)
            break;

        case 7:                     // Options
            SHOW(editSettings)
            break;

        default:
            ui->tabWidget->clear();
            break;
    }
}

MDomDocument MainWindow::getXmlRequest()
{
    return xmlRequest;
}

void MainWindow::clearXmlRequest()
{
    xmlRequest.loock();
    xmlRequest.clear();

    QDomElement root = xmlRequest.createElement("request");
    xmlRequest.appendChild(root);

    advancedEditor->plainTextEdit->setPlainText(xmlRequest.toString());
}

bool MainWindow::setXmlRequest(QString data)
{

    qDebug()<<"MainWindow::updateXmlData() called"<<endl;
    QString errorMessage, line, column;
    int errorLine, errorColumn;

    MDomDocument tmp;

    if(!tmp.setContent(data, false, &errorMessage, &errorLine, &errorColumn))
    {
        line.setNum(errorLine);
        column.setNum(errorColumn);

        RaiseWarning * raiseWarning = new RaiseWarning(errorMessage.append(" at line ").append(line).append(" column ").append(column), this);
        raiseWarning->raise();
        return false;
    }

    xmlRequest.loock();
    xmlRequest.setContent(data, false);
    log(QString("Richiesta modificata. ").append(errorMessage));

    return true;
}

void MainWindow::doPush()
{
   qDebug() << "MainWindow::doPush() called" << endl;

   QString params("login=" + MGlob::dbUser() + "&pass=" + MGlob::dbPass() + "&stream=1&service=gui&xmlmessage="+xmlRequest.toString());

   connect(&httpPush, SIGNAL(done(bool)), this, SLOT(pushDone(bool)));

   QHttpRequestHeader header("POST", MGlob::dbPath());
   header.setValue("Host", MGlob::dbHost());
   header.setValue( "User-Agent", "User Agent");
   header.setContentType("application/x-www-form-urlencoded");

   httpPush.request(header, params.toUtf8());

}

void MainWindow::pushDone(bool error)
{
    if(error)
    {
        log(QString("error occurred").append(httpPush.errorString()));
        progressDialog->hide();
        return;
    }

    progressDialog->setValue(100);
    progressDialog->hide();

    log(QString(httpPush.readAll()));
    disconnect(&httpPush, SIGNAL(done(bool)), this, SLOT(pushDone(bool)));

    clearXmlRequest();
    switchAction(ui->listWidget->currentRow());
}

void MainWindow::appendData()
{

    xmlRequest.gelementById("ciccio"); // this is called only to update indexing list of xmlRequest
    int fileNum = xmlRequest.imagesLocal.length();

    if(fileNum == 0)
    {
        doPush();
        return;
    }

    ftpPush = new QFtp(this);

    actualFtpCommand = 0;
    totalFtpCommand = 0;

    progressDialog->show();

    connect(ftpPush, SIGNAL(dataTransferProgress(qint64, qint64)), this, SLOT(updateDataTransferProgress(qint64, qint64)));
    connect(ftpPush, SIGNAL(commandFinished(int, bool)), this, SLOT(ftpCommandFinished(int, bool)));

    ftpPush->connectToHost(MGlob::ftpHost(), MGlob::ftpPort());
    QTimer::singleShot(20000, this, SLOT(checkFtp()));
    ftpPush->login(MGlob::ftpUser(), MGlob::ftpPass());
    ftpPush->cd(MGlob::ftpBasePath());

    totalFtpCommand = fileNum;

    for(int i=0; i < fileNum; i++)
    {
        file = new QFile(xmlRequest.imagesLocal.at(i));
        if (!file->open(QIODevice::ReadOnly))
        {
              RaiseWarning * raiseWarning = new RaiseWarning("Apertura del file " + xmlRequest.imagesLocal.at(i) + " Fallita!", this);
              raiseWarning->raise();
              delete file;
              return;
        }

        ftpPush->put(file, xmlRequest.images.at(i));
    }

}

void MainWindow::ftpCommandFinished(int id, bool error)
{
    QString tot; tot.setNum(totalFtpCommand);
    QString act; act.setNum(actualFtpCommand);
    QString ids; ids.setNum(id);

    if(error)
    {
        progressDialog->hide();
        //qWarning() << "Error occurred at command " << act << " with id " << ids << " Error code " << ftpPush .error() <<"should mean:" << ftpPush.errorString() << endl;
        RaiseWarning * raiseWarning = new RaiseWarning("Error occurred :" + ftpPush->errorString(), this);
        raiseWarning->raise();
        ftpPush->abort();
        ftpPush->deleteLater();

        ftpPush = 0;

        return;
    }

    progressDialog->setWindowTitle("Trasferring File: " + act + " of " + tot);

    if(ftpPush->currentCommand() == QFtp::Put)
        actualFtpCommand++;

    if(actualFtpCommand == totalFtpCommand)
    {
        //SleeperThread::msleep(5000);
        doPush();
        progressDialog->setWindowTitle("Sending request to Database");
        progressDialog->setMaximum(100);
        progressDialog->setValue(99);
    }
}

void MainWindow::updateDataTransferProgress(qint64 readBytes, qint64 totalBytes)
{
     progressDialog->setMaximum(totalBytes);
     progressDialog->setValue(readBytes);
}

void MainWindow::log(QString message)
{
    QString logString;
    ui->logPlainTextEdit->appendPlainText(logString.setNum(logIndex).append(" :~ ").append(message));
    //qDebug() << logString.setNum(logIndex).append(" :~ ").append(message) << endl;
    logIndex++ ;
}

void MainWindow::checkFtp()
{
    if(ftpPush
       && ftpPush->state() != QFtp::Connected
       && ftpPush->state() != QFtp::LoggedIn
       && ftpPush->state() != QFtp::Closing
       )
    {
        progressDialog->hide();
        //qWarning() << "Error occurred at command " << act << " with id " << ids << " Error code " << ftpPush .error() <<"should mean:" << ftpPush.errorString() << endl;
        RaiseWarning * raiseWarning = new RaiseWarning("Ftp connection timed out! with: " + ftpPush->errorString(), this);
        raiseWarning->raise();
        ftpPush->abort();
        ftpPush->deleteLater();
        ftpPush = 0;
    }
}
