/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include "loadcompo.h"
#include "mglob.h"
#include "mainwindow.h"

LoadCompo::LoadCompo(QWidget *parent, MainWindow *window) : LoadProd(parent)
{

    MINIT
    connect(pushButton_Img, SIGNAL(clicked()), this, SLOT(findImg()));

    setWindowTitle("Carica Composizioni");

    delete comboBox_Id;
    delete label_Id;
}

void LoadCompo::appendData()
{
    if(isNotValid()) return;

    OP("crea")

    QString imgLocal(lineEdit_Img->text());
    QString presente("0");
    if(checkBox_Presente->isChecked()) presente.setNum(1);

    QDomElement item(tmp.createElement("item"));

    item.setAttribute("table",              "prodotti");
    item.setAttribute("id",                 dbId);
    item.setAttribute("genere",             "p");
    item.setAttribute("nome",               lineEdit_Name->text());
    item.setAttribute("img",                "prod_" + dbId + imgLocal.right(imgLocal.size()- imgLocal.lastIndexOf(".")));
    item.setAttribute("imgLocal",           imgLocal);
    item.setAttribute("prezzo",             lineEdit_Price->text());
    item.setAttribute("prezzopromozionale", lineEdit_PromPrice->text());
    item.setAttribute("presente",           presente);
    item.setAttribute("descrizione",        textEdit_Descr->toPlainText());
    item.setAttribute("genere",             "c");

    tmp.loock();

    op.appendChild(item);

    item = tmp.createElement("item");

    item.setAttribute("table",              "prodoTipi");
    item.setAttribute("idProd",             dbId);
    item.setAttribute("idTipo",             comboBox_Tipo->itemData(comboBox_Tipo->currentIndex()).toString());

    op.appendChild(item);
}

