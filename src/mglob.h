/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#ifndef MGLOB_H
#define MGLOB_H

#include <QString>
#include <QDomElement>
#include <QtDebug>
#include <QHttpRequestHeader>

#include "mdomdocument.h"
#include "raiseWarning.h"


namespace MGlob
{
    QString dbHost();
    quint16 dbPort();
    QString dbPath();
    QString dbUser();
    QString dbPass();
    QString dbCACert();
    QString ftpHost();
    quint16 ftpPort();
    QString ftpUser();
    QString ftpPass();
    QString ftpBasePath();
    QString emptyQString();
}


#define WARNV(message) \
{ \
  RaiseWarning * raiseWarning = new RaiseWarning(message, mainWindow); \
  raiseWarning->raise(); \
  return true; \
}

#define SHOW(widget) \
{ \
  widget->asNew(); \
  ui->tabWidget->addTab(widget, widget->windowTitle()); \
}

#define DOQUERY doQuery(QString query) \
{ \
   QString params("login=" + MGlob::dbUser() + "&pass=" + MGlob::dbPass() + QString("&stream=1&service=gui&xmlmessage="+ query).replace("+","%2B")); \
   connect(&httpQuery, SIGNAL(done(bool)), this, SLOT(queryDone(bool))); \
   QHttpRequestHeader header("POST", MGlob::dbPath()); \
   header.setValue("Host", MGlob::dbHost()); \
   header.setValue("User-Agent", "User Agent"); \
   header.setContentType("application/x-www-form-urlencoded"); \
   httpQuery.setHost(MGlob::dbHost(), MGlob::dbPort()); \
   httpQuery.request(header, params.toUtf8()); \
}

#define QUERYDONE queryDone(bool error)\
{ \
    if(error) \
    { \
        qCritical() << QString("Error occurred: ").append(httpQuery.errorString()).append(httpQuery.readAll()); \
        return; \
    } \
    QString data = httpQuery.readAll(); \
    xmlData.loock(); \
    xmlData.setContent(data, false); \
    emit xmlDataUpdated(); \
    disconnect(&httpQuery, SIGNAL(done(bool)), this, SLOT(queryDone(bool))); \
}


#define OP(ID) \
    MDomDocument tmp = mainWindow->getXmlRequest(); \
    tmp.loock(); \
    QDomElement root = tmp.firstChildElement("request"); \
    QDomElement op = tmp.gelementById(ID); \
    if(op.isNull()) \
        { \
            op = tmp.createElement("op"); \
            op.setAttribute("id", ID); \
            root.appendChild(op); \
        }

#define MINIT \
    setupUi(this); \
    mainWindow = window; \
    setAttribute(Qt::WA_DeleteOnClose); \
    connect(pushButton_Apply, SIGNAL(clicked()), this, SLOT(appendData())); \
    connect(pushButton_Cancel, SIGNAL(clicked()), this, SLOT(asNew()));

#endif // MGLOB_H
