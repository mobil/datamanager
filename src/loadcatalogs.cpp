/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QDate>
#include <QtDebug>

#include "loadcatalogs.h"
#include "mdomdocument.h"
#include "mainwindow.h"
#include "mglob.h"

LoadCatalogs::LoadCatalogs(QWidget *parent, MainWindow *window)
         : QWidget(parent)
{
    MINIT
}

LoadCatalogs::~LoadCatalogs()
{}

void LoadCatalogs::appendData()
{
    if(isNotValid()) return;

    OP("crea")

    QDomElement item = tmp.createElement("item");
    item.setAttribute("table",        "cataloghi");
    item.setAttribute("nomeCatalogo", nameLineEdit->text());
    item.setAttribute("dtInizValid" , initDateEdit->date().toString(Qt::ISODate));
    item.setAttribute("dtFineValid" , endDateEdit->date().toString(Qt::ISODate));
    item.setAttribute("tipoCatalogo", typeComboBox->currentText().left(1).toLower());

    op.appendChild(item);
}

void LoadCatalogs::asNew()
{
    initDateEdit->setDate(QDate::currentDate());
    endDateEdit->setDate(QDate::fromString("7999-12-31", Qt::ISODate));
}

bool LoadCatalogs::isNotValid()
{
    if(nameLineEdit->text() == "") WARNV("Specifica il nome del catalogo");

    return false;
}
