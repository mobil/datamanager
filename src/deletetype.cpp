/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include "deletetype.h"
#include "mainwindow.h"
#include "mglob.h"

DeleteType::DeleteType(QWidget *parent) : LoadType(parent)
{}

DeleteType::DeleteType(QWidget *parent, MainWindow *window) : LoadType(parent)
{
    MINIT

    dbTableName = "tipi";
    dbIdAttr    = "idTipo";
    dbNameAttr  = "nomeTipo";

    setWindowTitle("Elimina Tipi");

    delete comboBox_Child;
    delete label_Child;
    delete lineEdit_Name;
    delete label_Name;
}

void DeleteType::asNew()
{
    xmlData.clear();
    idWhere.clear();
    comboBox_id->clear();

    connect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));

    doQuery("<request><op id='view'><item table='" + dbTableName + "' /></op></request>");
}

void DeleteType::populate()
{
   QDomNodeList items(xmlData.elementsByTagName("item"));

   for(int i=0; i<items.size(); i++ )
   {
       if(items.at(i).isElement())
       {
           QDomElement item(items.at(i).toElement());
           QString nome(item.attributeNode(dbNameAttr).value());
           QString id(item.attributeNode(dbIdAttr).value());

           idWhere.insert(id, item.attributeNode("where").value());
           comboBox_id->addItem(id+" - "+nome, id);
       }
   }
   disconnect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));
}

void DeleteType::appendData()
{
    if(isNotValid()) return;

    OP("cancella")

    QDomElement item(tmp.createElement("item"));

    item.setAttribute("table",              dbTableName);
    item.setAttribute("nome" ,              comboBox_id->currentText());
    item.setAttribute("where",              idWhere[comboBox_id->itemData(comboBox_id->currentIndex()).toString()]);

    tmp.loock();

    op.appendChild(item);
}

bool DeleteType::isNotValid()
{
    if(comboBox_id->currentText() == "1 - root") WARNV("Seleziona un tipo diverso da 'root'");

    return false;
}
