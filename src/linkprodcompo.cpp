/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include "linkprodcompo.h"
#include "mglob.h"
#include "mainwindow.h"

LinkProdCompo::LinkProdCompo(QWidget *parent, MainWindow *window) : LinkProdCatalog(parent)
{
    MINIT

    setWindowTitle("Prodotti-Composizioni");

    tabLabels      << "Composizione" << "Associato" << "Associa" << "Disassocia" << "id Composizione";
    dbTableName    = "prodotti";
    dbTableAssName = "prodoInComposizione";
    dbAssoAttr     = "idComposizione";
    dbNameAttr     = "nome";
    idCol          = 4;

    connect(comboBox_ProdName, SIGNAL(activated(int)), this, SLOT(fill(int)));
}

void LinkProdCompo::populate()
{

   QTableWidgetItem * textItem = new QTableWidgetItem();
   textItem->setFlags(Qt::ItemIsSelectable |Qt::ItemIsEnabled);

   QTableWidgetItem * checkItem = new QTableWidgetItem();
   checkItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
   checkItem->setCheckState(Qt::Checked);

   QDomNodeList items(xmlData.elementsByTagName("item"));

   for(int i=0; i<items.size(); i++ )
   {
       if(items.at(i).isElement())
       {
           QDomElement item(items.at(i).toElement());

           QString idProd(item.attributeNode("id").value());
           if(idProd != "")
           {
               QString name(idProd + " - " + item.attributeNode("nome").value());
               comboBox_ProdName->addItem(name, idProd);
           }

           if(item.attributeNode("table").value() == dbTableName && item.attributeNode("genere").value() == "c")
           {
               tableWidget_Catalogs->insertRow(tableWidget_Catalogs->rowCount());

               int actualRow = tableWidget_Catalogs->rowCount() -1;

               QTableWidgetItem * it1 = textItem->clone();  it1->setText(item.attributeNode(dbNameAttr).value());
               QTableWidgetItem * it2 = checkItem->clone(); it2->setCheckState(Qt::Unchecked);
               QTableWidgetItem * it3 = checkItem->clone(); it3->setCheckState(Qt::Unchecked);
               QTableWidgetItem * it4 = checkItem->clone(); it4->setCheckState(Qt::Unchecked);
               QTableWidgetItem * it5 = textItem->clone();  it5->setText(item.attributeNode("id").value());

               tableWidget_Catalogs->setItem(actualRow, 0, it1);
               tableWidget_Catalogs->setItem(actualRow, 1, it2);
               tableWidget_Catalogs->setItem(actualRow, 2, it3);
               tableWidget_Catalogs->setItem(actualRow, 3, it4);
               tableWidget_Catalogs->setItem(actualRow, 4, it5);

           }
       }
   }

   tableWidget_Catalogs->resizeColumnsToContents();

   disconnect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));
}
