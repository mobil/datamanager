/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QtDebug>
#include <QDomNodeList>

#include "mdomdocument.h"


MDomDocument::MDomDocument() : QDomDocument()
{
    isUpToDate = false;
}
MDomDocument::MDomDocument(QString name) : QDomDocument(name)
{
    isUpToDate = false;
}
MDomDocument::MDomDocument(QDomDocumentType doctype) : QDomDocument(doctype)
{
    isUpToDate = false;
}
MDomDocument::MDomDocument(QDomDocument x) : QDomDocument(x)
{
    isUpToDate = false;
}

MDomDocument::~MDomDocument(){}

QDomElement MDomDocument::gelementById(QString elementId)
{
    if(!isUpToDate)
    {
        visita(*this);
        isUpToDate = true;
    }

    QDomElement fnullElement;

    return indexer.value(elementId, fnullElement);
}

void MDomDocument::visita(QDomNode node)
{

    if(node.isElement())
    {
        QDomElement tmp = node.toElement();

        if(!tmp.attributeNode("imgLocal").isNull() && tmp.attributeNode("imgLocal").value() != tmp.attributeNode("img").value())
        {
            imagesLocal.append(tmp.attributeNode("imgLocal").value());
            images.append(tmp.attributeNode("img").value());
        }

        if(!tmp.attributeNode("id").isNull())
            indexer.insert(tmp.attributeNode("id").value(), tmp);
    }

    QDomNodeList childrens = node.childNodes();

    for(uint i=0; i < childrens.length(); i++ )
        visita(childrens.at(i));
}

void MDomDocument::loock()
{
    isUpToDate = false;
    indexer.clear();
    images.clear();
    imagesLocal.clear();
}
