/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QFileDialog>
#include <QtDebug>

#include "loadtrademarks.h"
#include "mainwindow.h"
#include "mglob.h"

LoadTrademarks::LoadTrademarks(QWidget *parent, MainWindow *window) : QWidget(parent), Ui::loadTrademarkForm()
{
    MINIT

    connect(pushButton_Img, SIGNAL(clicked()), this, SLOT(findImg()));
}

LoadTrademarks::~LoadTrademarks()
{}

void LoadTrademarks::findImg()
{
    qDebug() << "LoadTrademarks::setImgPath() called"  << endl;
    QFileDialog * fileDialog = new QFileDialog(mainWindow, Qt::Window);
    fileDialog->setWindowModality(Qt::ApplicationModal);
    fileDialog->setAttribute(Qt::WA_DeleteOnClose);
    connect(fileDialog, SIGNAL(fileSelected(QString)), this, SLOT(setImgPath(QString)));
    fileDialog->show();
}

void LoadTrademarks::setImgPath(QString imgPath)
{
    lineEdit_Img->setText(imgPath);
}

void LoadTrademarks::appendData()
{
    OP("crea")

    QDomElement item = tmp.createElement("item");

    QString imgLocal(lineEdit_Img->text());
    QString id; id.setNum(dbId);

    item.setAttribute("table", "marchi");
    item.setAttribute("nomeMarchio", lineEdit_Name->text());
    item.setAttribute("img",                "mark_" + id + imgLocal.right(imgLocal.size()- imgLocal.lastIndexOf(".")));
    item.setAttribute("imgLocal",           imgLocal);

    tmp.loock();

    op.appendChild(item);

    dbId++;
}

void LoadTrademarks::asNew()
{
    lineEdit_Name->clear();
    lineEdit_Img->clear();

    connect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));

    doQuery("<request><op id='raw'><item q=\"SELECT IF( ISNULL( MAX( id ) ) , 1, max( id ) +1 ) AS newId FROM `marchi`\" /></op></request>");
}

bool LoadTrademarks::isNotValid()
{
    if(lineEdit_Name->text() == "") WARNV("Specifica il nome")
    if(lineEdit_Img->text()  == "") WARNV("Specifica l'immagine")

    return false;
}

void LoadTrademarks::DOQUERY

void LoadTrademarks::QUERYDONE

void LoadTrademarks::populate()
{
    QDomNodeList items(xmlData.elementsByTagName("item"));

    for(int i=0; i<items.size(); i++ )
    {
       if(items.at(i).isElement())
       {
           QDomElement item(items.at(i).toElement());

           if(item.attributeNode("newId").value() != "")
           {
               bool ok;
               dbId = item.attributeNode("newId").value().toUInt(&ok);
           }
       }
    }

    disconnect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));
}
