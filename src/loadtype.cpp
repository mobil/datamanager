/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QtDebug>

#include "loadtype.h"
#include "mainwindow.h"
#include "mglob.h"

LoadType::LoadType(QWidget *parent) : QWidget(parent)
{}

LoadType::LoadType(QWidget *parent, MainWindow *window) : QWidget(parent), Ui::loadTypeForm()
{
    MINIT

    delete comboBox_id;
    delete label_id;
}

void LoadType::DOQUERY

void LoadType::QUERYDONE

void LoadType::populate()
{

   QDomNodeList items(xmlData.elementsByTagName("item"));

   for(int i=0; i<items.size(); i++ )
   {
       if(items.at(i).isElement())
       {
           QDomElement item(items.at(i).toElement());
           QString nomeTipo(item.attributeNode("nomeTipo").value());
           QString path(item.attributeNode("path").value());
           bool ok;
           int id = item.attributeNode("idTipo").value().toInt(&ok);
           if(ok)
           {
               if( maxId < id) maxId = id;
               comboBox_Child->addItem(nomeTipo, path);
           }
       }
   }
   disconnect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));
}

void LoadType::appendData()
{
    if(isNotValid()) return;

    OP("crea")

    //make node
    QString idtipo; idtipo.setNum(++maxId);
    QDomElement item = tmp.createElement("item");

    item.setAttribute("table",    "tipi");
    item.setAttribute("nomeTipo", lineEdit_Name->text());
    item.setAttribute("idTipo",   idtipo);
    item.setAttribute("path",     comboBox_Child->itemData(comboBox_Child->currentIndex()).toString() + "." + idtipo );

    //append node
    op.appendChild(item);

    tmp.loock();
}

void LoadType::asNew()
{
    maxId = 0;
    xmlData.clear();
    comboBox_Child->clear();

    connect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));

    doQuery("<request><op id='view'><item table='tipi' /></op></request>");
}

bool LoadType::isNotValid()
{
    if(lineEdit_Name->text() == "" ) WARNV("Specifica il nome");

    return false;
}

LoadType::~LoadType()
{}

