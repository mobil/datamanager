/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QSsl>
#include <QSslSocket>
#include <QSslCertificate>
#include <QSslError>
#include <QIODevice>
#include <QFile>

#include "mhttps.h"
#include "mglob.h"
#include "raiseWarning.h"

MHttps::MHttps(QObject * parent) : QHttp(parent)
{
    QFile certFile(MGlob::dbCACert());
    certFile.open(QIODevice::ReadOnly);
    QSslCertificate cert(&certFile, QSsl::Pem);
    QSslSocket * sslSocket = new QSslSocket(this); // This replaces the internal QTcpSocket QHttp uses, unfortunately we cannot reuse that one because Qt does not provide an accessor for it
    sslSocket->addCaCertificate(cert);
    setSocket(sslSocket);

    connect(this, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(handleSslError(QList<QSslError>)));
}

MHttps::MHttps(QString hostName, quint16 port, QObject * parent) : QHttp( hostName, QHttp::ConnectionModeHttps, port, parent)
{
    QFile certFile(MGlob::dbCACert());
    certFile.open(QIODevice::ReadOnly);
    QSslCertificate cert(&certFile, QSsl::Pem);
    QSslSocket * sslSocket = new QSslSocket(this); // This replaces the internal QTcpSocket QHttp uses, unfortunately we cannot reuse that one because Qt does not provide an accessor for it
    sslSocket->addCaCertificate(cert);
    setSocket(sslSocket);

    connect(this, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(handleSslError(QList<QSslError>)));
}

int MHttps::setHost(QString hostName, quint16 port)
{
    return QHttp::setHost(hostName, QHttp::ConnectionModeHttps, port);
}

void MHttps::handleSslError(QList<QSslError> errors)
{
    for(int i=0; i < errors.length(); i++)
    {
        switch (errors.at(i).error())
        {
            case QSslError::HostNameMismatch :
            case QSslError::NoSslSupport :
            {
                RaiseWarning * raiseWarning = new RaiseWarning("Connessione Interrotta! Qualcuno ha tentato di manomettere la connessione\n" + errors.at(i).errorString());
                raiseWarning->raise();
                qWarning() << errors.at(i).errorString() << endl;
                break;
            }
            default:
                RaiseWarning * raiseWarning = new RaiseWarning("Connessione Interrotta! Qualcuno ha tentato di manomettere la connessione\n" + errors.at(i).errorString());
                raiseWarning->raise();
                qWarning() << errors.at(i).errorString() << endl;
                //ignoreSslErrors();
                break;
        }
    }
}
