/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QSettings>
#include <QtDebug>
#include <QVariant>
#include <QFileDialog>

#include "editSettings.h"
#include "raiseWarning.h"
#include "mainwindow.h"
#include "mglob.h"


EditSettings::EditSettings(QWidget *parent, MainWindow *window) : QWidget(parent), Ui::EditSettingsUi()
{
    MINIT

    connect(this->pushButton_Cert, SIGNAL(clicked()), this, SLOT(findCACert()));
}

EditSettings::~EditSettings()
{}

void EditSettings::appendData(){

    if(isNotValid()) return;

    QSettings settings("G104S4lv0","mqs");

    settings.setValue("dbHostName", hostLineEdit->text());
    settings.setValue("dbPort", portLineEdit->text());
    settings.setValue("dbUser", userLineEdit->text());
    settings.setValue("dbPass", passLineEdit->text());
    settings.setValue("dbName", dbLineEdit->text());
    settings.setValue("dbCACert", lineEdit_Cert->text());

    settings.setValue("ftpHostName", lineEdit_FtpHost->text());
    settings.setValue("ftpPort", lineEdit_FtpPort->text());
    settings.setValue("ftpUser", lineEdit_FtpUser->text());
    settings.setValue("ftpPass", lineEdit_FtpPass->text());
    settings.setValue("ftpBasePath", lineEdit_FtpBasePath->text());

}

void EditSettings::asNew(){

    hostLineEdit->setText(MGlob::dbHost());
    portLineEdit->setText(QVariant(MGlob::dbPort()).toString());
    userLineEdit->setText(MGlob::dbUser());
    passLineEdit->setText(MGlob::dbPass());
    confirmLineEdit->setText(MGlob::dbPass());
    dbLineEdit->setText(MGlob::dbPath());
    lineEdit_Cert->setText(MGlob::dbCACert());

    lineEdit_FtpHost->setText(MGlob::ftpHost());
    lineEdit_FtpPort->setText(QVariant(MGlob::ftpPort()).toString());
    lineEdit_FtpUser->setText(MGlob::ftpUser());
    lineEdit_FtpPass->setText(MGlob::ftpPass());
    lineEdit_FtpConfirm->setText(MGlob::ftpPass());
    lineEdit_FtpBasePath->setText(MGlob::ftpBasePath());
}

bool EditSettings::isNotValid()
{
    if(hostLineEdit->text() == "") WARNV("Devi specificare l' host!");
    if(portLineEdit->text() == "") WARNV("Devi specificare la porta");
    if(userLineEdit->text() == "") WARNV("Devi specificare l' utente");
    if(dbLineEdit->text()   == "") WARNV("Devi specificare il database in uso");
    if(passLineEdit->text() == "") WARNV("Devi specificare la password");
    if(lineEdit_Cert->text() == "") WARNV("Devi specificare il certificato della Ceritification Authority");

    if(lineEdit_FtpHost->text()     == "") WARNV("Devi specificare l' host! ftp");
    if(lineEdit_FtpPort->text()     == "") WARNV("Devi specificare la porta ftp");
    if(lineEdit_FtpUser->text()     == "") WARNV("Devi specificare l' utente ftp");
    if(lineEdit_FtpPass->text()     == "") WARNV("Devi specificare la password ftp");
    if(lineEdit_FtpBasePath->text() == "") WARNV("Devi specificare il path di base");

    if(passLineEdit->text()     != confirmLineEdit->text())     WARNV("La password e la conferma non corrispondono")
    if(lineEdit_FtpPass->text() != lineEdit_FtpConfirm->text()) WARNV("La password ftp e la conferma non corrispondono")

    return false;
}

void EditSettings::findCACert()
{
    qDebug() << "EditSettings::findCACert() called"  << endl;
    QFileDialog * fileDialog = new QFileDialog(mainWindow, Qt::Window);
    fileDialog->setWindowModality(Qt::ApplicationModal);
    fileDialog->setAttribute(Qt::WA_DeleteOnClose);
    connect(fileDialog, SIGNAL(fileSelected(QString)), this, SLOT(setCACertPath(QString)));
    fileDialog->show();
}

void EditSettings::setCACertPath(QString CACertPath)
{
    lineEdit_Cert->setText(CACertPath);
}
