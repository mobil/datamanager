/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QtDebug>
#include <QTableWidgetItem>
#include <QHeaderView>
#include <QDate>

#include "linkprodcatalog.h"
#include "mainwindow.h"
#include "mglob.h"

LinkProdCatalog::LinkProdCatalog(QWidget *parent) : QWidget(parent)
{
    tabLabels.clear();
}

LinkProdCatalog::LinkProdCatalog(QWidget *parent, MainWindow *window) : QWidget(parent)
{
    MINIT

    tabLabels.clear();
    tabLabels      << "Catalogo" << "Associato" << "Associa" << "Disassocia" << "Tipo" << "Inizio" << "Fine" <<"id Catalogo";
    dbTableName    = "cataloghi";
    dbTableAssName = "prodoCataloghi";
    dbAssoAttr     = "idCatalogo";
    dbNameAttr     = "nomeCatalogo";
    idCol          = 7;

    connect(comboBox_ProdName, SIGNAL(activated(int)), this, SLOT(fill(int)));
}

void LinkProdCatalog::DOQUERY

void LinkProdCatalog::QUERYDONE

void LinkProdCatalog::populate()
{

   QTableWidgetItem * textItem = new QTableWidgetItem();
   textItem->setFlags(Qt::ItemIsSelectable |Qt::ItemIsEnabled);

   QTableWidgetItem * checkItem = new QTableWidgetItem();
   checkItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
   checkItem->setCheckState(Qt::Checked);

   QDomNodeList items(xmlData.elementsByTagName("item"));

   for(int i=0; i<items.size(); i++ )
   {
       if(items.at(i).isElement())
       {
           QDomElement item(items.at(i).toElement());

           QString idProd(item.attributeNode("id").value());
           if(idProd != "")
           {
               QString name(idProd + " - " + item.attributeNode("nome").value());
               comboBox_ProdName->addItem(name, idProd);
           }

           if(item.attributeNode("table").value() == dbTableName)
           {
               tableWidget_Catalogs->insertRow(tableWidget_Catalogs->rowCount());

               int actualRow = tableWidget_Catalogs->rowCount() -1;

               QString catalogType;
               switch(item.attributeNode("tipoCatalogo").value()[0].toAscii())
               {
                   case 'd':
                   {
                       catalogType = "Catalogo";
                       break;
                   }
                   case 'p':
                   {
                       catalogType = "Promozione";
                       break;
                   }
               }

               QTableWidgetItem * it1 = textItem->clone();  it1->setText(item.attributeNode(dbNameAttr).value());
               QTableWidgetItem * it2 = checkItem->clone(); it2->setCheckState(Qt::Unchecked);
               QTableWidgetItem * it3 = checkItem->clone(); it3->setCheckState(Qt::Unchecked);
               QTableWidgetItem * it4 = checkItem->clone(); it4->setCheckState(Qt::Unchecked);
               QTableWidgetItem * it5 = textItem->clone();  it5->setText(catalogType);
               QTableWidgetItem * it6 = textItem->clone();  it6->setText(QDate::fromString(item.attributeNode("dtInizValid").value(), "yyyy-MM-dd").toString("dd-MM-yyyy"));
               QTableWidgetItem * it7 = textItem->clone();  it7->setText(QDate::fromString(item.attributeNode("dtFineValid").value(), "yyyy-MM-dd").toString("dd-MM-yyyy"));
               QTableWidgetItem * it8 = textItem->clone();  it8->setText(item.attributeNode(dbAssoAttr).value());


               tableWidget_Catalogs->setItem(actualRow, 0, it1);
               tableWidget_Catalogs->setItem(actualRow, 1, it2);
               tableWidget_Catalogs->setItem(actualRow, 2, it3);
               tableWidget_Catalogs->setItem(actualRow, 3, it4);
               tableWidget_Catalogs->setItem(actualRow, 4, it5);
               tableWidget_Catalogs->setItem(actualRow, 5, it6);
               tableWidget_Catalogs->setItem(actualRow, 6, it7);
               tableWidget_Catalogs->setItem(actualRow, 7, it8);

           }
       }
   }

   tableWidget_Catalogs->resizeColumnsToContents();

   disconnect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));
}

void LinkProdCatalog::fill(int index)
{
   idWhere.clear();

   int actualRow;
   int totalRow = tableWidget_Catalogs->rowCount();   

   for(actualRow = 0; actualRow < totalRow; actualRow++)
   {
       tableWidget_Catalogs->item(actualRow , 1)->setCheckState(Qt::Unchecked);
       tableWidget_Catalogs->item(actualRow , 2)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);
       tableWidget_Catalogs->item(actualRow , 2)->setCheckState(Qt::Unchecked);
       tableWidget_Catalogs->item(actualRow , 3)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled );
       tableWidget_Catalogs->item(actualRow , 3)->setCheckState(Qt::Unchecked);
   }

   QDomNodeList items(xmlData.elementsByTagName("item"));

   for(int i=0; i<items.size(); i++ )
   {
       if(items.at(i).isElement())
       {
           QDomElement item(items.at(i).toElement());

           if(item.attributeNode("table").value() == dbTableAssName)
           {
               QList<QTableWidgetItem *> lista = tableWidget_Catalogs->findItems(item.attributeNode(dbAssoAttr).value(), Qt::MatchExactly);
               for(int i=0 ; i < lista.count(); i++)
               {
                   QTableWidgetItem * matchItem = lista.at(i);
                   if(matchItem->column() == idCol)
                   {
                       int actualRow = matchItem->row();

                       if(item.attributeNode("idProd").value() == comboBox_ProdName->itemData(index))
                       {
                          tableWidget_Catalogs->item(actualRow, 1)->setCheckState(Qt::Checked);
                          tableWidget_Catalogs->item(actualRow, 2)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
                          tableWidget_Catalogs->item(actualRow, 2)->setCheckState(Qt::Unchecked);
                          tableWidget_Catalogs->item(actualRow, 3)->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemIsUserCheckable);

                          idWhere.insert(item.attributeNode(dbAssoAttr).value(), item.attributeNode("where").value());
                       }
                   }
               }
           }
       }
   }

   tableWidget_Catalogs->setDisabled(false);
}

void LinkProdCatalog::appendData()
{
    int rowCount = tableWidget_Catalogs->rowCount();
    for(int i=0; i < rowCount; i++)
    {
        if(tableWidget_Catalogs->item(i,2)->checkState() == Qt::Checked)
        {
            OP("crea")

            QDomElement item = tmp.createElement("item");
            item.setAttribute("table",       dbTableAssName);
            item.setAttribute("idProd",      comboBox_ProdName->itemData(comboBox_ProdName->currentIndex()).toString());
            item.setAttribute(dbAssoAttr ,   tableWidget_Catalogs->item(i, idCol)->text());

            op.appendChild(item);
        }
        else if(tableWidget_Catalogs->item(i,3)->checkState() == Qt::Checked)
        {
            OP("cancella")

            QDomElement item = tmp.createElement("item");
            item.setAttribute("table",       dbTableAssName);
            item.setAttribute("where",       idWhere[tableWidget_Catalogs->item(i, idCol)->text()]);

            op.appendChild(item);
        }
    }

}

void LinkProdCatalog::asNew()
{
    xmlData.clear();
    idWhere.clear();
    comboBox_ProdName->clear();

    tableWidget_Catalogs->setDisabled(true);
    tableWidget_Catalogs->clear();
    tableWidget_Catalogs->setColumnCount(idCol +1);
    tableWidget_Catalogs->setRowCount(0);
    tableWidget_Catalogs->setHorizontalHeaderLabels(tabLabels);
    tableWidget_Catalogs->verticalHeader()->hide();

    connect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));

    doQuery("<request><op id='view'><item table='" +dbTableAssName + "' /><item table='" + dbTableName + "' /><item table='prodotti' /></op></request>");
}

