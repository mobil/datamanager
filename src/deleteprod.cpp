/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include "deleteprod.h"
#include "mainwindow.h"
#include "mglob.h"

DeleteProd::DeleteProd(QWidget *parent, MainWindow *window) : EditProd(parent)
{
    MINIT

    setWindowTitle("Elimina Prodotti");

    lineEdit_PromPrice->setDisabled(true);
    lineEdit_Price->setDisabled(true);
    lineEdit_Name->setDisabled(true);
    lineEdit_Img->setDisabled(true);
    textEdit_Descr->setDisabled(true);
    checkBox_Presente->setDisabled(true);
    comboBox_Tipo->setDisabled(true);
    pushButton_Img->setDisabled(true);

    connect(pushButton_Img, SIGNAL(clicked()), this, SLOT(findImg()));
    connect(comboBox_Id, SIGNAL(activated(int)), this, SLOT(fill(int)));
}

void DeleteProd::appendData()
{
    if(isNotValid()) return;

    OP("cancella")

    QDomElement item(tmp.createElement("item"));

    item.setAttribute("table",              "prodotti");
    item.setAttribute("nome",               lineEdit_Name->text());
    item.setAttribute("where",              whereProd);

    tmp.loock();

    op.appendChild(item);
/*
    item = tmp.createElement("item");

    item.setAttribute("table",              "prodoTipi");
    item.setAttribute("where",              whereType);

    op.appendChild(item);
*/
}
