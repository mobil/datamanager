/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QSettings>
#include <QtDebug>

#include "mglob.h"

QSettings settings(QString("G104S4lv0"),QString("mqs"));

QString MGlob::dbHost()
{
    return settings.value("dbHostName", "localhost").toString();
}

quint16 MGlob::dbPort()
{
    bool ok;
    return (quint16) settings.value("dbPort","443").toUInt(&ok);
}

QString MGlob::dbPath()
{
    return settings.value("dbName","/mqs/test/param.php").toString();
}

QString MGlob::dbUser()
{
    return settings.value("dbUser","").toString();
}

QString MGlob::dbPass()
{
    return settings.value("dbPass","").toString();
}

QString MGlob::dbCACert()
{
    return settings.value("dbCACert","").toString();
}

QString MGlob::ftpHost()
{
    return settings.value("ftpHostName","localhost").toString();
}

quint16 MGlob::ftpPort()
{
    bool ok;
    return (quint16) settings.value("ftpPort","21").toUInt(&ok);
}

QString MGlob::ftpUser()
{
    return settings.value("ftpUser","").toString();
}

QString MGlob::ftpPass()
{
    return settings.value("ftpPass","").toString();
}

QString MGlob::ftpBasePath()
{
    return settings.value("ftpBasePath","").toString();
}

QString MGlob::emptyQString()
{
    QString emptyqstring("");
    return emptyqstring;
}
