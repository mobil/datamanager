/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#ifndef MHTTPS_H
#define MHTTPS_H

#include <QHttp>
#include <QList>
#include <QSslError>
#include <QObject>
#include <QString>

class MHttps : public QHttp
{
    Q_OBJECT

    public:
        MHttps(QObject * parent = 0 );
        MHttps(QString hostName, quint16 port = 443, QObject * parent = 0 );
        int setHost(QString hostName, quint16 port = 443 );

    private slots:
        void handleSslError(QList<QSslError> errors);

};

#endif // MHTTPS_H
