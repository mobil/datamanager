/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#ifndef OPTIONS_H_
#define OPTIONS_H_

#include "ui_ui_editSettings.h"

#include <QWidget>

class MainWindow;

class EditSettings : public QWidget, public Ui::EditSettingsUi{

	Q_OBJECT

	public:
                EditSettings(QWidget *parent, MainWindow *window);
		~EditSettings();

        public slots:
            void asNew();

        private slots:
                void appendData();
                void findCACert();
                void setCACertPath(QString CACertPath);

        private:
                MainWindow * mainWindow;
                bool isNotValid();

};


#endif /* OPTIONS_H_ */
