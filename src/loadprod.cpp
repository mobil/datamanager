/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QFileDialog>

#include "loadprod.h"
#include "mainwindow.h"
#include "mglob.h"

LoadProd::LoadProd(QWidget *parent) : QWidget(parent)
{}

LoadProd::LoadProd(QWidget *parent, MainWindow *window) : QWidget(parent)
{
    MINIT

    setWindowTitle("Carica Prodotti");

    delete comboBox_Id;
    delete label_Id;

    connect(pushButton_Img, SIGNAL(clicked()), this, SLOT(findImg()));
}


void LoadProd::DOQUERY

void LoadProd::QUERYDONE

void LoadProd::populate()
{
    QDomNodeList items(xmlData.elementsByTagName("item"));

    for(int i=0; i<items.size(); i++ )
    {
       if(items.at(i).isElement())
       {
           QDomElement item(items.at(i).toElement());

           if(item.attributeNode("newId").value() != "")
           {
               bool ok;
               dbId = item.attributeNode("newId").value().toUInt(&ok);
           }
           QString idTipo(item.attributeNode("idTipo").value());
           QString nomeTipo(item.attributeNode("nomeTipo").value());
           if( idTipo != "" && nomeTipo != "")
               comboBox_Tipo->addItem(nomeTipo, idTipo);
       }
    }

    disconnect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));
}

void LoadProd::appendData()
{
    if(isNotValid()) return;

    OP("crea")

    QString idb; idb.setNum(dbId);
    QString imgLocal(lineEdit_Img->text());
    QString presente("0");
    if(checkBox_Presente->isChecked()) presente.setNum(1);

    QDomElement item(tmp.createElement("item"));

    item.setAttribute("table",              "prodotti");
    item.setAttribute("id",                 idb);
    item.setAttribute("genere",             "p");
    item.setAttribute("nome",               lineEdit_Name->text());
    item.setAttribute("img",                "prod_" + idb + imgLocal.right(imgLocal.size()- imgLocal.lastIndexOf(".")));
    item.setAttribute("imgLocal",           imgLocal);
    item.setAttribute("prezzo",             lineEdit_Price->text());
    item.setAttribute("prezzopromozionale", lineEdit_PromPrice->text());
    item.setAttribute("presente",           presente);
    item.setAttribute("descrizione",        textEdit_Descr->toPlainText().replace("\"","%26quot;"));

    tmp.loock();

    op.appendChild(item);

    item = tmp.createElement("item");

    item.setAttribute("table",              "prodoTipi");
    item.setAttribute("idProd",             idb);
    item.setAttribute("idTipo",             comboBox_Tipo->itemData(comboBox_Tipo->currentIndex()).toString());

    op.appendChild(item);

    dbId++;

}

void LoadProd::asNew()
{
    dbId = 0;
    xmlData.clear();

    lineEdit_PromPrice->clear();
    lineEdit_Price->clear();
    lineEdit_Name->clear();
    lineEdit_Img->clear();
    textEdit_Descr->clear();
    checkBox_Presente->setChecked(false);
    comboBox_Tipo->clear();

    connect(this, SIGNAL(xmlDataUpdated()), this, SLOT(populate()));

    doQuery("<request><op id='raw'><item q=\"SELECT IF( ISNULL( MAX( id ) ) , 1, max( id ) +1 ) AS newId FROM `prodotti`\" /></op><op id='view'><item table='tipi' /></op></request>");
}

void LoadProd::findImg()
{
    qDebug() << "LoadTrademarks::setImgPath() called"  << endl;
    QFileDialog * fileDialog = new QFileDialog(mainWindow, Qt::Window);
    fileDialog->setWindowModality(Qt::ApplicationModal);
    fileDialog->setAttribute(Qt::WA_DeleteOnClose);
    connect(fileDialog, SIGNAL(fileSelected(QString)), this, SLOT(setImgPath(QString)));
    fileDialog->show();
}

void LoadProd::setImgPath(QString imgPath)
{
    lineEdit_Img->setText(imgPath);
}

bool LoadProd::isNotValid()
{
    if(lineEdit_Name->text()         == "" )     WARNV("Specifica il nome");
    if(lineEdit_Img->text()          == "" )     WARNV("Seleziona un'immagine");
    if(textEdit_Descr->toPlainText() == "" )     WARNV("Scrivi la descirzione");
    if(comboBox_Tipo->currentText()  == "root" ) WARNV("Specifica un tipo diverso da 'root'");

    return false;
}
