/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QWidget>
#include <QListWidgetItem>
#include <QFtp>
#include <QFile>
#include <QProgressDialog>
//#include <QThread>

#include "mdomdocument.h"
#include "mhttps.h"
#include "ui_ui_mainwindow.h"
#include "loadprod.h"
#include "loadcompo.h"
#include "loadtype.h"
#include "loadtags.h"
#include "loadtrademarks.h"
#include "loadcatalogs.h"
#include "editSettings.h"
#include "editprod.h"
#include "linkprodcatalog.h"
#include "linkprodcompo.h"
#include "linkprodtag.h"
#include "linkprodmark.h"
#include "deleteprod.h"
#include "deletetype.h"
#include "deletetag.h"
#include "deletecatalog.h"
#include "deletetrademark.h"
#include "advancededitor.h"



class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void log(QString message);

    bool setXmlRequest(QString data);
    MDomDocument getXmlRequest();


public slots:
    void clearXmlRequest();



signals:
    void xmlDataUpdated();


private:
    QProgressDialog * progressDialog;

    MDomDocument xmlRequest;
    MHttps        httpPush;

    QFile        * file;
    QFtp         * ftpPush;
    bool         errorOnPush;
    uint         actualFtpCommand;
    uint         totalFtpCommand;

    uint logIndex;

    Ui::MainWindow          * ui;
    LoadProd                * loadProd;
    LoadCompo               * loadCompo;
    LoadType                * loadType;
    LoadTags                * loadTags;
    LoadTrademarks          * loadTrademarks;
    LoadCatalogs            * loadCatalogs;
    EditProd                * editProd;
    EditSettings            * editSettings;
    LinkProdCatalog         * linkProdCatalog;
    LinkProdCompo           * linkProdCompo;
    LinkProdTag             * linkProdTag;
    LinkProdMark            * linkProdMark;
    DeleteProd              * deleteProd;
    DeleteType              * deleteType;
    DeleteTag               * deleteTag;
    DeleteTradeMark         * deleteTradeMark;
    DeleteCatalog           * deleteCatalog;
    AdvancedEditor          * advancedEditor;





private slots:
    void switchAction(int currentRow);
    void doPush();
    void updateDataTransferProgress(qint64, qint64);
    void ftpCommandFinished(int id, bool error);
    void pushDone(bool error);
    void appendData();
    void checkFtp();


};

/*
class SleeperThread : public QThread
{
public:
    static void msleep(unsigned long msecs)
    {
    QThread::msleep(msecs);
    }
};
*/

#endif // MAINWINDOW_H
