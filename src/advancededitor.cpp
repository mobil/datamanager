/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#include <QDebug>

#include "advancededitor.h"
#include "mainwindow.h"
#include "mdomdocument.h"
#include "mglob.h"

AdvancedEditor::AdvancedEditor(QWidget *parent, MainWindow *window)
         : QWidget(parent), Ui::advancedEditingForm()
{
    MINIT

    connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(awared(int)));
}

AdvancedEditor::~AdvancedEditor(){}

void AdvancedEditor::awared(int state)
{
    if(state == Qt::Checked)
    {
        plainTextEdit->setReadOnly(false);
        return;
    }

    plainTextEdit->setReadOnly(true);

}

void AdvancedEditor::appendData()
{
    mainWindow->setXmlRequest(plainTextEdit->toPlainText());
}

void AdvancedEditor::asNew()
{
    plainTextEdit->setReadOnly(true);
    plainTextEdit->setPlainText(mainWindow->getXmlRequest().toString());

    checkBox->setCheckState(Qt::Unchecked);
}
