/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#ifndef LINKPRODCATALOG_H
#define LINKPRODCATALOG_H

#include <QWidget>
#include <QMap>
#include <QString>
#include <QTableWidgetItem>

#include "ui_ui_linkProduct.h"
#include "mdomdocument.h"
#include "mhttps.h"

class MainWindow;

class LinkProdCatalog : public QWidget, public Ui::productLinkForm
{
    Q_OBJECT

    public:
        LinkProdCatalog(QWidget *parent);
        LinkProdCatalog(QWidget *parent, MainWindow *window);

    public slots:
        virtual void asNew();

    protected:
        MainWindow *mainWindow;
        MHttps httpQuery;
        MDomDocument xmlData;
        QString dbTableName;
        QString dbTableAssName;
        QString dbAssoAttr;
        QString dbNameAttr;
        int    idCol;
        QStringList tabLabels;

        QMap<QString,QString> idWhere;

        void doQuery(QString query);

    protected slots:
        virtual void populate();
        virtual void appendData();
        void fill(int index);
        void queryDone(bool error);

    signals:
        void xmlDataUpdated();
};

#endif // LINKPRODCATALOG_H
