/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#ifndef LOADTRADEMARKS_H
#define LOADTRADEMARKS_H

#include <QWidget>

#include "ui_ui_trademarkLoad.h"
#include "mdomdocument.h"
#include "mhttps.h"

class MainWindow;

class LoadTrademarks : public QWidget, public Ui::loadTrademarkForm
{

    Q_OBJECT

    public:
        LoadTrademarks(QWidget *parent, MainWindow *window);
        ~LoadTrademarks();

    public slots:
        void asNew();

    private slots:
        void queryDone(bool error);
        void populate();
        void findImg();
        void appendData();
        void setImgPath(QString imgPath);

    private:
        uint      dbId;
        MHttps        httpQuery;
        MDomDocument xmlData;

        MainWindow * mainWindow;

        bool isNotValid();
        void doQuery(QString query);

    signals:
        void xmlDataUpdated();

};

#endif // LOADTRADEMARKS_H
