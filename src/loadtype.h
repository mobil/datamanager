/*

Copyright (C) 2007,2009  Gioacchino Mazzurco <gmazzurco89@gmail.com>

This file is a part of Mobil Quattro Sud CMS.

Mobil Quattro Sud CMS is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Mobil Quattro Sud CMS is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Mobil Quattro Sud CMS.  If not, see <http://www.gnu.org/licenses/>.

For license details read COPYING.txt .
For all other info read README.txt .

*/

#ifndef LOADTYPE_H
#define LOADTYPE_H

#include <QWidget>
#include <QMap>

#include "ui_ui_typeLoad.h"
#include "mdomdocument.h"
#include "mhttps.h"

class MainWindow;

class LoadType : public QWidget, public Ui::loadTypeForm
{
    Q_OBJECT

    public:
        LoadType(QWidget *parent);
        LoadType(QWidget *parent, MainWindow *window);
        ~LoadType();

    public slots:
        virtual void asNew();

    protected:

        MainWindow * mainWindow;
        MHttps        httpQuery;
        MDomDocument xmlData;
        int          maxId;

        void doQuery(QString query);
        virtual bool isNotValid();


    protected slots:
        virtual void appendData();
        virtual void populate();
        void queryDone(bool error);

    signals:
        void xmlDataUpdated();
};

#endif // LOADTYPE_H
