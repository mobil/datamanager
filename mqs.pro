# -------------------------------------------------
# Project created by QtCreator 2009-08-06T18:35:36
# -------------------------------------------------
QT += network \
    svg \
    xml
TARGET = mqs
TEMPLATE = app
OBJECTS_DIR = build/
MOC_DIR = build/
UI_DIR = build/
RCC_DIR = build/
SOURCES += src/main.cpp \
    src/mainwindow.cpp \
    src/loadtype.cpp \
    src/advancededitor.cpp \
    src/loadtags.cpp \
    src/loadtrademarks.cpp \
    src/loadcatalogs.cpp \
    src/mdomdocument.cpp \
    src/mglob.cpp \
    src/editSettings.cpp \
    src/raiseWarning.cpp \
    src/linkprodcatalog.cpp \
    src/linkprodcompo.cpp \
    src/loadprod.cpp \
    src/loadcompo.cpp \
    src/editprod.cpp \
    src/linkprodtag.cpp \
    src/deleteprod.cpp \
    src/deletetype.cpp \
    src/deletetag.cpp \
    src/deletetrademark.cpp \
    src/linkprodmark.cpp \
    src/deletecatalog.cpp \
    src/mhttps.cpp
HEADERS += src/mainwindow.h \
    src/loadtype.h \
    src/advancededitor.h \
    src/loadtags.h \
    src/loadtrademarks.h \
    src/loadcatalogs.h \
    src/mdomdocument.h \
    src/mglob.h \
    src/editSettings.h \
    src/raiseWarning.h \
    src/linkprodcatalog.h \
    src/linkprodcompo.h \
    src/loadprod.h \
    src/loadcompo.h \
    src/editprod.h \
    src/linkprodtag.h \
    src/deleteprod.h \
    src/deletetype.h \
    src/deletetag.h \
    src/deletetrademark.h \
    src/linkprodmark.h \
    src/deletecatalog.h \
    src/mhttps.h
FORMS += ui/ui_mainwindow.ui \
    ui/ui_advancedEditing.ui \
    ui/ui_catalogLoad.ui \
    ui/ui_linkProduct.ui \
    ui/ui_trademarkLoad.ui \
    ui/ui_typeLoad.ui \
    ui/ui_editSettings.ui \
    ui/ui_warning.ui \
    ui/ui_product.ui
RESOURCES += rsrc.qrc
